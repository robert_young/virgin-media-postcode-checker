# Virgin Media Postcode Checker

Checks the Virgin Media website every day at 12pm to see if your house is available!

- AWS Lambda
- AWS SES
- Serverless
- Docker

## Test locally

```sh
AWS_ACCESS_KEY_ID="YOUR_KEY" AWS_SECRET_ACCESS_KEY="YOU_SECRET" docker-compose up
```

## Deploy

```sh
serverless deploy -v
```