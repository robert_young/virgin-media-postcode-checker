const webdriver = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

const AWS = require('aws-sdk');

const ses = new AWS.SES({
    region: 'eu-west-1'
});

process.env.PATH = `${process.env.PATH}:/opt/lib`;
process.env.PATH = `${process.env.PATH}:/${process.cwd()}/lib`;

async function takeScreenshot(driver, filename) {
    const ss = await driver.takeScreenshot();
    require('fs').writeFileSync(filename, ss, 'base64');
}

function sendEmail(message, to) {
    return new Promise((resolve, reject) => {
        const eParams = {
            Destination: {
                ToAddresses: [to]
            },
            Message: {
                Body: {
                    Text: {
                        Data: message
                    }
                },
                Subject: {
                    Data: "Virgin Media Postcode Checker"
                }
            },
            Source: to
        };

        console.log('===SENDING EMAIL===');

        ses.sendEmail(eParams, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                console.log("===EMAIL SENT===");
                resolve();

            }
        });
    })
}

async function acceptCookies(driver) {
    try {
        await driver.findElement(webdriver.By.id('consent_prompt_submit')).click();
    } catch (err) { }
}

async function check(event, context, callback) {
    try {
        const builder = new webdriver.Builder().forBrowser('chrome');
        const chromeOptions = new chrome.Options();
        const defaultChromeFlags = [
            '--headless',
            '--disable-gpu',
            '--window-size=1280x1696', // Letter size
            '--no-sandbox',
            '--user-data-dir=/tmp/user-data',
            '--hide-scrollbars',
            '--enable-logging',
            '--log-level=0',
            '--v=99',
            '--single-process',
            '--data-path=/tmp/data-path',
            '--ignore-certificate-errors',
            '--homedir=/tmp',
            '--disk-cache-dir=/tmp/cache-dir'
        ];

        chromeOptions.addArguments(defaultChromeFlags);
        builder.setChromeOptions(chromeOptions);

        const driver = builder.build();

        await driver.get(process.env.URL_POSTCODE_CHECKER);
        await acceptCookies(driver);
        await driver.findElement(webdriver.By.id('postcode-field')).sendKeys(event.postcode, webdriver.Key.RETURN);
        await driver.wait(webdriver.until.elementIsVisible(driver.findElement(webdriver.By.css('.address-results'))), 5000);
        await acceptCookies(driver);

        const addressList = await driver.findElements(webdriver.By.css(".address-list > li"))

        for (let addressIndex = 0; addressIndex < addressList.length; addressIndex += 1) {
            const address = addressList[addressIndex];
            const addressText = await address.getText();

            console.log(addressText);

            if (addressText.includes(event.fullAddress)) {
                console.log(`Found ${addressIndex}`);
                await acceptCookies(driver);
                // await takeScreenshot(driver, "ss.png");
                await address.click();
                break;
            }

            if (addressIndex === addressList.length - 1) {
                throw new Error("Address not found")
            }
        }

        await driver.findElement(webdriver.By.id('next')).click();
        await driver.sleep(5000);

        const url = await driver.getCurrentUrl();

        await driver.quit();

        let message = '';

        if (url.startsWith(process.env.URL_SERVICE_AVAILABLE)) {
            message = 'Services are available! :)';
            // TODO: Push to SQS
            // await sendEmail("Services are available! :)", event.email);
        } else if (url.startsWith(process.env.URL_SERVICE_UNAVAILABLE)) {
            message = 'Services are not available! :(';
            // TODO: Push to SQS
            // await sendEmail("Services are not available! :(", event.email);
        }

        if (!message) {
            callback("Unable to determine");
        } else {
            await sendEmail(message, event.email)
            // TODO: Push to SQS
            callback(null, message);
        }
    } catch (err) {
        console.error(err);
        await sendEmail("Something bad has happened!", event.email);
        callback(err);
    }
};

module.exports.check = check;