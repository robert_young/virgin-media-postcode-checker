const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const sqs = new AWS.SQS();

async function sendEmailMessageBatch(params) {
    return new Promise((resolve, reject) => {
        sqs.sendMessageBatch(params, (err, data) => {
            if (err) {
                console.log(err, err.stack); // an error occurred
                return reject(err);
            }
            
            console.log(data);
            return resolve(data);
        });
    });
}

function notify(event, context, callback) {
    const { email, success } = event;

    const params = {
        TableName: process.env.DYNAMODB_TABLE,
        Key: {
            email,
        },
    };

    dynamoDb.get(params, async (error, result) => {
        if (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: { 'Content-Type': 'text/plain' },
                body: 'Couldn\'t fetch the subscriber item.',
            });
            return;
        }

        const emailEntries = result.Item.notifications
            .filter(notification => notification.type === "email")
            .map(notification => ({
                success,
                email: notification.email,
            }))

        await sendEmailMessageBatch({
            QueueUrl: process.env.SQS_EMAIL_QUEUE,
            Entries: emailEntries,
        });

        callback(true);
    });
}

module.exports.notify = notify;