const AWS = require('aws-sdk');

const ses = new AWS.SES({
    region: 'eu-west-1'
});

function sendEmail(message, to) {
    return new Promise((resolve, reject) => {
        const eParams = {
            Destination: {
                ToAddresses: [to]
            },
            Message: {
                Body: {
                    Text: {
                        Data: message
                    }
                },
                Subject: {
                    Data: "Virgin Media Postcode Checker"
                }
            },
            Source: to
        };

        console.log('===SENDING EMAIL===');
        
        ses.sendEmail(eParams, (err) => {
            if (err) reject(err);
            else {
                console.log("===EMAIL SENT===");
                resolve();

            }
        });
    })
}

async function send(event, context, callback) {
    const { success, to } = event;
    let message = "Services are not available! :(";

    if (success === true) {
        message = "Services are available! :)";
    }

    await sendEmail(message, to);

    callback(null, message);
}


module.exports.send = send;